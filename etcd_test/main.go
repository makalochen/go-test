package main

import (
	"context"
	"fmt"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

func main() {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"127.0.0.1:22379"},
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		fmt.Printf("连接失败，错误信息：%+v", err)
	}
	defer cli.Close()
	// 监听 test2
	// 派一个哨兵 一直监视 key 的变化（新增，修改，删除)
	ch := cli.Watch(context.Background(), "test2")
	// 尝试重通道取值（监视信息）
	for wresp := range ch {
		for _, evt := range wresp.Events {
			fmt.Printf("type:%v; key:%v; value:%v \n", evt.Type, string(evt.Kv.Key), string(evt.Kv.Value))
		}
	}

}
