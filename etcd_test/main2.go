package main

import (
	"context"
	"fmt"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

func main() {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"127.0.0.1:22379"},
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		fmt.Printf("连接失败，错误信息：%+v", err)
	}
	defer cli.Close()

	// 创建客户端这个回话存活 2 s
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)

	// put 新增
	_, err = cli.Put(ctx, "test2", "新增")
	cancel()
	if err != nil {
		fmt.Printf("put 操作失败, err:%v\n", err)
		return
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	// put 修改
	_, err = cli.Put(ctx, "test2", "修改")
	cancel()
	if err != nil {
		fmt.Printf("put 操作失败, err:%v\n", err)
		return
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	// get 获取
	resp, err := cli.Get(ctx, "test2")
	cancel()
	if err != nil {
		fmt.Printf("get 操作失败, err:%v\n", err)
		return
	}
	for _, ev := range resp.Kvs {
		fmt.Printf("%s:%s\n", ev.Key, ev.Value)
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	// 删除
	_, err = cli.Delete(ctx, "test2")
	cancel()
	if err != nil {
		panic(err.Error())
	}
}
