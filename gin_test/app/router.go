package gin_test

import "github.com/gin-gonic/gin"

// 控制器方法
func hello(context *gin.Context) {
	context.JSON(200, "这是app模块")
}

// 设置路由
func Routers(router *gin.Engine) {
	// 设置路由组
	App := router.Group("/App")
	{
		// 注册路由
		App.GET("/hello", hello)
	}
}
