package gin_test

import "github.com/gin-gonic/gin"

// 控制器方法
func hello(context *gin.Context) {
	context.JSON(200, "这是 Common 模块")
}

// 设置路由
func Routers(router *gin.Engine) {
	// 设置路由组
	Common := router.Group("/Common")
	{
		// 注册路由
		Common.GET("/hello", hello)
	}
}
