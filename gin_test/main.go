package main

import (
	"fmt"

	// 引入自定义路由模块
	app "gin_test/app"
	common "gin_test/common"
	routers "gin_test/routers"
	web "gin_test/web"
)

func main() {
	// 加载所有模块的路由配置
	routers.Include(app.Routers, web.Routers, common.Routers)
	// 初始化路由
	router := routers.Init()
	// 启动监听并打印错误
	if err := router.Run(); err != nil {
		fmt.Println(err)
		return
	}
}
