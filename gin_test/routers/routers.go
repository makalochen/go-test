package routers

import (
	"github.com/gin-gonic/gin"
)

// 定义单个路由model
type RouterModel func(router *gin.Engine)

// 路由model 切片
var routerModels = make([]RouterModel, 0)

// 注册路由model配置
func Include(models ...RouterModel) {
	// 塞到路由模块切片中
	routerModels = append(routerModels, models...)
}

// 初始化路由
func Init() *gin.Engine {
	router := gin.Default()
	for _, model := range routerModels {
		model(router)
	}
	return router
}
