package gin_test

import (
	// 控制器方法分开
	webCon "gin_test/controller"

	"github.com/gin-gonic/gin"
)

// 控制器方法
/* func hello(context *gin.Context) {
	context.JSON(200, "这是 Web 模块")
} */

// 设置路由
func Routers(router *gin.Engine) {
	// 设置路由组
	Web := router.Group("/Web")
	{
		// 注册路由
		Web.GET("/hello", webCon.Hello)
	}
}
